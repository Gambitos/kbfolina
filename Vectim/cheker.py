from winreg import *
import wmi

if 'KB5022498' in str(wmi.WMI().query('SELECT HotFixID FROM Win32_QuickFixEngineering')):
    print('FALSE')
else:
    try:
        OpenKey(ConnectRegistry(None, HKEY_CLASSES_ROOT), r'ms-msdt')
        print('TRUE')
    except OSError:
        print('FALSE')

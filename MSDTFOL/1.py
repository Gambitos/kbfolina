import os
from flask import Flask, send_file

app = Flask(__name__)

@app.route("/Folina")
def send_map():
    startdir = os.path.abspath(os.curdir)
    requested_path = os.path.relpath('follina.doc', startdir)
    requested_path = os.path.abspath(requested_path)
    return send_file(requested_path, as_attachment=False)
 
if __name__ == '__main__':
    app.run(host = "0.0.0.0",port=8080,debug=True) 

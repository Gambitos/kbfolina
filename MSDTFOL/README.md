# MS-MSDT "Follina" Attack Vector

> За основу взят скрипт John Hammond Follina

--------------



# Usage

```
python3 script.py
**внутри скрита script.py необходимо добавить нужные команды:
options:
   -c COMMAND
                        command to run on the target (default: calc)
   -o OUTPUT
                        output maldoc file (default: ./follina.doc)
   -i INTERFACE
                        network interface or IP address to host the HTTP server (default: eth0)
   -p PORT  port to serve the HTTP server (default: 8000)

```
`

![Reverse Shell](https://user-images.githubusercontent.com/6288722/171037880-03a73d6a-4606-4c42-abcb-ee52a9e669c6.png)